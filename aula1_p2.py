from math import pi

# Metros segundo para quilómetro hora
ms = int(input('Digite o quilômetro/hora: '))
km = ms * 3.6
print(f'O resultado convertido é: {km:.2f}Km/H\n')

# Milhas convertida em Quilômetros
milhas = float(input('Digite as milhas: '))
km = 1.61 * milhas
print(f'{milhas} milhas corresponde em quilômetros a: {km:.2f}Km\n')

# Quilômetros convertida em Milhas
km = float(input('Digite os quilômetros: '))
milhas = km / 1.61
print(f'{km} quilômetros corresponde em milhas a: {milhas:.2f}M\n')

# Ângulo em graus convertido em radianos
g = float(input('Digite os ângulo em graus: '))
r = g * pi / 180
print(f'O resultado é: {r:.3f} radianos\n')

# Ângulo em radiano convertido em graus
r = float(input('Digite os ângulo em radianos: '))
g = r * 180 / pi
print(f'O resultado é: {round(g):.3f} graus\n')

# Polegadas para centímetros
p = float(input('Digite o valor em polgadas: '))
cm = p * 2.54
print(f'O valor em centímetros é: {cm:.2f}Cm\n')

# Centímetros para polegadas
cm = float(input('Digite o valor em centímetros: '))
p = cm / 2.54
print(f'O valor em polegadas é: {p:.2f}\n')

# Volume em metros cúbicos para litros
vol_metro = float(input('Digite seu valor em metros cúbicos: '))
const = 1000
ltr = const * vol_metro
print(f'O valor em litros corresponde a: {ltr:.2f}\n')

# Volume em litros para metros cúbicos
const = 1000
ltr = float(input('Digite os litros: '))
vol_metro = ltr / const
print(f'O valor em metros cúbicos é: {vol_metro:.2f}\n')

# Massa em quilogramas para libras
kg = float(input('Digite o valor em quilogramas: '))
lb = kg / 0.45
print(f'O resultado em libras é: {lb:.2f}\n')
