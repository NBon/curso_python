import math

#----------------------------------------- 1 -----------------------------------------#

priNum = int(input('Digite o primeiro número: '))
segNum  = int(input('Digite o segundo número : ')) 

maior = priNum

if (segNum > maior):
    maior = segNum

print(f'O número maior é: {maior}\n')


#----------------------------------------- 2 -----------------------------------------#

num = int(input('Digite um número: '))

resultado = num % 2

if resultado == 0:
    raiz = math.sqrt(num)
    print(f'A raiz quadrada de {num} é: {raiz:.4f}\n')
else:
    print('Número é inválido.')


#----------------------------------------- 3 -----------------------------------------#

numReal = float(input('Digite um número: '))

resultado = numReal % 2

if resultado == 0:
    raiz = math.sqrt(numReal)
    print(f'A raiz quadrada de {numReal} é: {raiz:.4f}')
elif numReal > 0:
    numReal **= 2
    print(f'O quadrado é: {numReal:.2f}\n')


#----------------------------------------- 4 -----------------------------------------#

num = float(input('Digite um numero: '))

resultado = num % 2
quadrado = num ** 2
raiz = math.sqrt(num)

if resultado > 0:
    print('Por favor, digite um numero positivo!')
elif resultado == 0:
    print(f'O número quadrado de {num} é: {quadrado}\n')
    print(f'A raiz quadrada de {num} é: {raiz:.4f}\n')


#----------------------------------------- 5 -----------------------------------------#

num = int(input('Digite um número: '))

resultado = num % 2

if resultado == 0:
    print ('O número digitado é par.\n')
else:
    print ('O númeor digitado é impar.\n')


#----------------------------------------- 6 -----------------------------------------#

priNumero = int(input('Digite o primero número: '))
segNumero = int(input('Digite o segundo número: '))

print('')
 
if priNumero > segNumero:
    print(f'O número maior é: {priNumero}\n')
else:
    print(f'O número maior é: {segNumero}\n')

if priNumero > segNumero:
    resultado = priNumero - segNumero
else:
    resultado = segNumero - priNumero

if priNumero > segNumero:
    print(f'A diferença entre o número {priNumero} e o número {segNumero} é: {resultado}\n')
else:
    print(f'A diferença entre o número {segNumero} e o número {priNumero} é: {resultado}\n')


#----------------------------------------- 7 -----------------------------------------#
   
priNumero = int(input('Digite o primero número: '))
segNumero = int(input('Digite o segundo número: '))

print('')
 
if priNumero > segNumero:
    print(f'O número maior é: {priNumero}\n')
elif priNumero < segNumero:
    print(f'O número maior é: {segNumero}\n')
else:
    print(f'Números iguais.\n')
   
   
#----------------------------------------- 8 -----------------------------------------#

priNota = float(input('Diguite a primera nota do aluno: '))
segNota = float(input('Diguite a segunda nota do aluno: '))

print('')

if 10 >= priNota >=0 and 10 >= segNota >= 0:
    media = (priNota + segNota) / 2
    print(f'A média do aluno é: {media:.2f}\n')
else:
    print('Nota não válida, por favor, digite uma nota valida ente 0.0 e 10.0\n')


#----------------------------------------- 9 -----------------------------------------#

salario = float(input('Digite o valor do salário: '))
prestacao = float(input('Digite o valor da prestação: '))

print('')

calculo = salario * 0.20

if prestacao > calculo:
    print('Empréstimo não concedido.\n')
else:
    print('Empréstimo concedido.\n')


#----------------------------------------- 10 -----------------------------------------#

altura = float(input('Digite a altura: '))
sexo = input('insira o seu sexo: m para masculino ou f para feminino: ')

print('')

if sexo == 'm':
    pesoIdela = (72.7 * altura) - 58
    print(f'O peso ideal para o homen com altura de {altura}m é: {pesoIdela:.2f}Kg\n')
else:
    pesoIdela = (62.1 * altura) - 44.7
    print(f'O peso ideal para a mulher com altura de {altura}m é: {pesoIdela:.2f}Kg\n')


#----------------------------------------- 11 -----------------------------------------#

num = int(input('Digite um numero: '))
if num > 0:
    num0 = num // 1 % 10
    num1 = num // 10 % 10
    num2 = num // 100 % 10
    num3 = num // 1000 % 10
    soma = num0 + num1 + num2 + num3
    print('O valor da soma dos algarismos é: ' + str(soma))
else:
    print('Numero Invalido!\n')


#----------------------------------------- 12 -----------------------------------------#

num = int(input('Digite um numero:'))
if num > 0:
    print(math.log(num))
else:
    print('Numero invalido!')


#----------------------------------------- 13 -----------------------------------------#

priNota = float(input('Digite a primeira nota: '))
segNota = float(input('Digite a segunda nota: '))
terNota = float(input('Digite a terceira nota: '))

media = (((priNota * 1) + (segNota * 1) + (terNota * 2)) / 4)
print(f'A media ponderada do aluno é: {media:.2f}\n')

if media > 6:
    print('O aluno foi aprovado!\n')
else:
    media < 6
    print('O aluno foi reprovado!\n')
    
    
#----------------------------------------- 14 -----------------------------------------#

labNota = float(input('Digite a nota do trabalho de laboratório: '))
semNota = float(input('Digite a nota da avaliação semestral: '))
finNota = float(input('Digite a nota da avaliação final: '))

media = (((labNota * 2) + (semNota * 3) + (finNota * 5)) / 10)

print('')

if media <= 2.9:
    print('O aluno foi reprovado!\n')
elif media <= 4.9:
    print('O aluno está de recuperação!\n')
else:
    print('O aluno está aprovado!\n')

print(f'A media do aluno é: {media:.1f}\n')


#----------------------------------------- 15 -----------------------------------------#

diaNum = int(input('Digite um numero para saber o dia da semana: '))

print('')

if diaNum == 1:
    print('Domingo\n')
elif diaNum == 2:
    print('Segunda-feira\n')
elif diaNum == 3:
    print('Terça-feira\n')
elif diaNum == 4:
    print('Quarta-feira\n')
elif diaNum == 5:
    print('Quinta-feira\n')
elif diaNum == 6:
    print('Sexta-feira\n')
elif diaNum == 7:
    print('Sabado\n')
else:
    print('Numero Invalido! Digite um numero de 1 a 7')


#----------------------------------------- 16 -----------------------------------------#

mesNum = int(input('Digite um numero para saber o mes do ano: '))

print('')

if mesNum == 1:
    print('Janeiro\n')
elif mesNum == 2:
    print('Fevereiro\n')
elif mesNum == 3:
    print('Março\n')
elif mesNum == 4:
    print('Abril\n')
elif mesNum == 5:
    print('Maio\n')
elif mesNum == 6:
    print('Junho\n')
elif mesNum == 7:
    print('Julho\n')
elif mesNum == 8:
    print('Agosto\n')
elif mesNum == 9:
    print('Setembro\n')
elif mesNum == 10:
    print('Outubro\n')
elif mesNum == 11:
    print('Novembro\n')
elif mesNum == 12:
    print('Dezembro\n')
else:
    print('Numero Invalido! Digite um numero de 1 a 12')


#----------------------------------------- 17 -----------------------------------------#

baseMenor = float(input('Digite o valor da base menor: '))
baseMaior = float(input('Digite o valor da base maior: '))
altura = float(input('Digite o valor da altura: '))

print('')

if 0 < baseMenor and baseMenor > 0 and  0 < baseMaior and baseMaior > 0 and 0 < altura and altura > 0:
    area = (((baseMaior + baseMenor) * altura) / 2)
    print(f'A area do trapezio é: {area:.2f}\n')
else:
    print('Nenhum valor pode ser igual ou menor que 0')
