# Primerio exercicio...
var1 = int(input('Digite um número inteiro: '))
print(f'Seu número digitado foi: {var1}\n')

# Segundo exercicio...
var2 = float(input('Digite um número real: '))
print(f'Seu número digitado foi: {var2}\n')

# Terceiro exercicio...
valor1 = int(input('Digite seu primero valor inteiro: '))
valor2 = int(input('Digite seu segundo valor inteiro: '))
valor3 = int(input('Digite seu terceiro valor inteiro: '))
soma = valor1 + valor2 + valor3
print(f'O resultado é: {soma}\n')

# Quarto exercicio...
real1 = float(input('Digite um número decimal: '))
quad = real1 ** 2
print(f'O quadrado de {real1} é: {quad:.2f}\n')

# Quinto exercicio...
real2 = float(input('Digite um número decimal: '))
qta = real2 / 5
print(f'A quinta parte de {real2} corresponde a: {qta:.2f}\n')

# Sexto exercicio...
cels = float(input('Digite os graus Celsios: '))
fah = cels * (9.0 / 5.0) + 32.0
print(f'A temperatura em graus Fahrenheit é: {fah:.2f}ºF\n')

# Setimo exercicio...
fah = float(input('Digite os graus Fahrenheit: '))
cels = 5.0 * (fah - 32.0) / 9.0
print(f'A temperatura em graus Celsios é: {cels:.2f}ºC\n')

# Oitavo exercicio...
kelv = float(input('Digite os graus Celsios: '))
cels = kelv - 273.15
print(f'A temperatura em graus Celsios é: {cels:.2f}ºC\n')

# Nono exercicio...
cels = float(input('Digite os graus Kelvin: '))
kelv = cels + 273.15
print(f'A temperatura em graus Celsios é: {kelv:.2f}ºK\n')

# Decimo exercicio...
km = int(input('Digite o quilômetro/hora: '))
ms = km / 3.6
print(f'O resultado {km} convertido em é: {ms:.2f}m/s\n')
